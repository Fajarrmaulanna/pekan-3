<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <form action="/welcome" method="post">
        @csrf
        <h3>Sign Up Form</h3>
        <p>First name :</p>
        <input type="text" name="FN" placeholder="Nama Depan" required>
        <p>Last name :</p>
        <input type="text" name="LN" placeholder="Nama Belakang">
        <p>Gender:</p>
        <input type="radio" name="gender" value="male">Male <br>
        <input type="radio" name="gender" value="female">Female <br>
        <input type="radio" name="gender" value="other">Other 

        <p>Nationality:</p>
        <select name="WN" required>
            <option value="1">Indonesian</option>
            <option value="2">Singapore</option>
            <option value="3">Malaysia</option>
            <option value="4">Other...</option>
        </select>
        <p>Language Spoken:</p>
        <input type="checkbox" >Bahasa Indonesia
        <br>
        <input type="checkbox" >English
        <br>
        <input type="checkbox" >Mandarin
        <br>
        <input type="checkbox" >Other

        <p>Bio:</p>
        <textarea name="bio" cols="30" rows="10"></textarea>
        <br>
        <button type="submit">Sign Up</button>
    </form>
</body>
</html> 