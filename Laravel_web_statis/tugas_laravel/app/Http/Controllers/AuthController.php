<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class AuthController extends Controller
{
    public function register(){
        return view('page.register');
    }
    public function kirim(Request $request){
        $firstName = $request['FN'];
        $lastName = $request['LN'];
        return view('page.welcome', compact('firstName','lastName'));
    }
    
}
