<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Cast;

class CastController extends Controller
{
    public function index(){

        //dengan builder query
        //$cast = DB::table('cast')->get();

        //dengan ORM 
        $cast = Cast::all();
        return view('cast.index', compact('cast'));
    }

    public function create(){
        return view ('cast.create');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);
        //dengan ORM
        $cast = Cast::create ([
            'nama' => $request->nama,
            'umur' => $request->umur,
            'bio' => $request->bio
        ]);

        return redirect('/cast');
    }

    //show
    public function show($id)
    {
        //$cast = DB::table('cast')->where('id', $id)->first();

        $cast = Cast::find($id);
        return view('cast.show', compact('cast'));
    }

    //edit
    public function edit($id)
    {
        // $cast = DB::table('cast')->where('id', $id)->first();

        $cast = Cast::find($id);

        return view('cast.edit', compact('cast'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        //query bulder
        // $query = DB::table('cast')
        //     ->where('id', $id)
        //     ->update([
        //         'nama' => $request["nama"],
        //         'umur' => $request["umur"],
        //         'bio' => $request["bio"]
        //     ]);

        //ORM Mass Updates
        //$update = Cast::where('id');

        $cast = Cast::find($id);
        $cast->nama = $request->nama;
        $cast->umur = $request->umur;
        $cast->bio = $request->bio;
        $cast->update();
        
        return redirect('/cast');
    }

    public function destroy($id)
    {
        Cast::destroy($id);
        return redirect('/cast');
    }
}
