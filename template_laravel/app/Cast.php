<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cast extends Model
{
    protected $table = 'cast';

    //bisa menggunakan fillable
    //protected $fillable = ['nama', 'umur', 'bio'];

    //bisa menggunakan guarded
    protected $guarded = [];

    public $timestamps = false;
}
