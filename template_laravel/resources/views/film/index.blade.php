@extends('layout.master')
@section('judul')
    Halaman List Film
@endsection

@section('isi')

<div class="row">
    @foreach($film as $item)
    <div class="col-4">
        <div class="card" style="width: 18rem;">
            <img class="card-img-top" src="{{asset('poster/' . $item->poster)}}" alt="Card image cap">
            <div class="card-body">
                <h5 class="card-title">{{$item->judul}}<br><br></h5>
                <p class="card-text">{{Str::limit ($item->ringkasan, 50)}}</p>
                <a href="#" class="btn btn-primary">Go somewhere</a>
            </div>
        </div>
    </div>
    @endforeach
</div>

@endsection