@extends('layout.master')
@section('judul')
    Halaman Edit Genre id : {{$genre->id}}
@endsection

@section('isi')

<div>
        <h2>Edit Genre {{$genre->id}}</h2>
        <form action="/genre/{{$genre->id}}" method="POST">
            @csrf
            @method('put')
            <div class="form-group">
                <label for="title">Nama</label>
                <input type="text" class="form-control" name="nama" value="{{$genre->nama}}" id="title" placeholder="Masukkan Nama">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Edit</button>
        </form>
    </div>

@endsection