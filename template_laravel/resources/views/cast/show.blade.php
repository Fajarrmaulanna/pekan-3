@extends('layout.master')
@section('judul')
    Halaman Show Cast id : {{$cast->id}}
@endsection

@section('isi')

<h2>Nama Cast : {{$cast->nama}}</h2>
<h4>Umur : {{$cast->umur}}</h4>
<p>Biodata <br>{{$cast->bio}}</p>

@endsection