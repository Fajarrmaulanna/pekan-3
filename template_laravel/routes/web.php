<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function (){
    return view('welcome');
});

Route::get('/data-table', function(){
    return view('tables.data-table');
});


Route::get('/table', function(){
    return view('tables.table');
});

//CRUD cast
// Route::get('/cast','CastController@index') -> name('cast.index');
// Route::get('/cast/create','CastController@create');
// Route::post('/cast','CastController@store');
// Route::get('/cast/{cast_id}','CastController@show');
// Route::get('/cast/{cast_id}/edit','CastController@edit');
// Route::put('/cast/{cast_id}','CastController@update');
// Route::delete('/cast/{cast_id}','CastController@destroy');

Route::resource('cast', 'CastController');

//CRUD Film
Route::resource('film', 'FilmController');

//CRUD genre
Route::resource('genre', 'GenreController');